# Kapitel 2: Verteilte DBS
## Unterschiede zwischen RDBMS und NoSQL
| RDBMS	| NoSQL	|
|---	|---	|
|   	|   	|
|   	|   	|
|---   	|---   	|

Verteilte Systeme ermöglichen **Skalierbarkeit**, **Fehlertoleranz** und **geringe Latenzzeiten** über *Partitionierung* und *Replikation*.
- Skalierbarkeit: wachsende/schrumpfende Datenmenge oder Anforderungen bzgl. Rechenzeit.
- Fehlertoleranz gegenüber Server-/Netzwerkfehler.
- geringe Latenzzeiten: kurze Antwortzeiten.

## Partitionierung  
Daten über mehrere Rechner verteilen.
+ Skalierbarkeit: Daten A-D auf Server1, E-H auf Server2, ...
+ gL: gleichzeitig Bearbeitung unterschiedlichen Anfragen.
+ weniger Speicherbedarf
- bei Ausfall einer Maschine, Teilmenge der Daten nicht erreichbar

horizontale Aufteilung der Daten (*Sharding*)
Kriterien:
- überwiegende lesende oder schreibende Zugriffe.
- welche Daten besonders häufig
- Häufigkeit und Dauer des Zugriffas
Eine gute Parittionierung impliziert:
- Nutzung von Lokalität.
- Minimierung der Kommunikation.
- Lastenausgleich.
Verschiedene Vorgehen:
- hash: injektive Funktion verteilt die Daten -> Vorteil: Lastenausgleich
- range: A-H auf Server1, I-J auf Server2, ...
wichtig ist, dass Daten, die oft zusammen zugegriffen sind, in dem selben Server gespeichert sind. Jedoch kann das zu inkorrekte Lastenausgleich führen. 

## Replikation  
Datem auf mehreren Rechner speichern.
+ Fehlertoleranz: kein Single-Point-of-Failure.
+ gL: gleichzeitig Bearbeitung unterschiedlichen Anfragen.
- Synchronisationsaufwand
- hoher Speicherbedarf
Arten:
- Master-Slave (Primary-Replica)
	- bei Primary: write, read, update zu Replicas
	- bei Replica: read
- Multi-Master (Multi-Primary)
	- write bei einem Primary -> synchronize
Replikation erfordet Kommunikation/Synchronisation

## Verfügbarkeit
Jede Anfrage wird garantiert beantwortet.
**Hochverfügbarkeit**: > 99.9% verfügbar.

## Serialisierbarkeit
- *strikte Serialisierbarkeit*: Goldstandard, alle Transaktionen erscheinen für alle Nutzer in der realen Zeit Reihenfolge.
	- benötigt Sperrprotokolle für alle Transaktionen
	- Blockierung bei Server-/Netzwerkfehler (geringe Verfügbarkeit)
- *Serialisierbarkeit*: (Abschwächung), alle Transaktionen erscheinen für alle Nutzer in derselben globalen Reihenfolge.
	- für rein schreibende Transaktionen: Mehrversionen-Synchronisation.
	- hat trotzdem selben Nachteile als Goldstandard.

### Konsistenzmodelle
Einschränkung: Transaktionen mit **einem Operator und einem Objekt**: r(x), update(x,v1)
- strikt Serialisierbar = *Linearisierbar*
- *sequentielle Konsistenz*: alle Operationen erscheinen für alle Nutzer in derselben globalen Reihenfolge (genau wie bei Serialisierbarkeit) und diese Reihenfolge ist konsistent mit den einzelnen Nutzersitzungen.
	- erfordert eine relativ aufwendige Synchornisation
	- vermindert Verfügbarkeit, erhöht Latenz

## BASE-Prinzip
Gegensatz zu sequentiell Konsistenz:
- BA: Basically Available = verfügbar
- S: Soft-State:
	- Werte immer vorläufig
	- Eintrage werden letzlich mit aktuelleren Werten überschrieben
	- Lesen alter Werte ist möglich (stale reads)
- E: Eventually Consistent
	- DB kann in inkonsistenten Zustand kommen: Kopien auf verschiedenen Replikaten für einen kurzen Zeitraum mit unterschiedlichen Werten
	- Letzendlich enthalten alle Kopien die gleichen Werte
	- Inconsistency Window

## Eventual Consistency
- Priorität auf Verfügbarkeit und Geschwindigkeit anstatt sequentielle Konsistenz
- Anfragen von nur einem Replikat bearbeiten (hohe Verfügbarkeit), danach (lazy/asynchron) an andere Replikate weiterleiten.
- in manchen Kontexten ist Verfügbarkeit wichtiger als Konsistenz
- Nachteile: Inkonsistenzen, Konflikten, Datenverlust, Aufwendung kann sich nicht auf Datenqualität verlassen -> mit verschieden Versionen umgehen.

## Sitzungsgarantien
- *Monotonic Reads MR*: man liest einen Wert, man kann danach nicht einen alteren Werte lesen.
- *Monotonic Write (MW)*: Nutzer1: update(x,v1), update(y,w1) Alle andere Nutzer nehmen die Schreibbefehle in der gleichen Reihenfolge wahr.
- *Writes Follow Reads (WFR)*: Nutzer0 u1: update(x,v1), Nutzer1 r(x) = v1, u2: update(y,w1) Alle andere Nutzer sehen Schreibbefehel u2 nach u1.
	- *MR*, *MW* und *WFR* sind möglich trotz hoher Verfügbarkeit
- *Read Your Writes (RYW)*: Jeder Nutzer liest die Werte seiner eigenen Schreibbefehle oder neuere Werte.
	- nicht mit hoher Verfügbarkeit vereinbar

## Kausale Konsistenz
Kompromiss zwischen sequentieller Konsistenz und Eventual Consitency
- Kausal abhängige Operationen sollten allen Nutzern in der gleichen Reihenfolge angezeigt werden.
- Die Reihenfolge kausal unabhängiger Operationen kann variieren.
- T2 ist **kausal abhängig** von T1 (T1 -> T2), falls
	- der selbe Nutzer führte T1 vor T2 aus,
	- T2 liest Werte, die durch T1 geschrieben wurden, oder
	- es gibt T3 mit T1 -> T3 und T3 -> T2 (Transitivität)
- Nutzer mit festen Replikat (oder lokaler Cache) verbunden ist, dann ist kausale Konsintenz mit hoher Verfügbarkeit vereinbar.
- Kausale Konsistenz gdw. MR, MW, WFR und RYW
- Sequentielle Konsistenz impliziert Kausale Konsistenz.

## ACID-Garantien
ACID-Eigenschaften können teilweise unter hoher Verfügbarkeit (HA) garantiert werden
- Atomicity: ja, über Verfahren ähnlich wie Sitzungsgarantien
	- Metadaten: Zeitstempel und Liste der aktualisierten Werte
	- Zurückhalten der Aktualisierung bis aktuelle Version aller Werte bei allen Replikaten angekommen
- Consistency: nein
	*Lost Updates* und *Write Skews* sind nicht unter HA vermeidbar
- Isolation: teilweise
	- logischer Einbenutzerbetrieb nicht mit HA vereinbar
	- schwächere Anforderungen mit HA möglich zu vereinbaren:
		- **Read Committed**: Transaktionen sollten keine Objekte lesen oder schreiben, die in einer anderen Transaktion aktualisiert werden und deren Commit noch aussteht.
		- **Item Cut Isolation**: das wiederholte Lesen des gleichen Objektes innerhalb einer Transaktion liefert stets den gleichen Wert.
- Durability: nein
	Damit eine Änderung den Ausfall von n Replikaten überlebt, müssen mindestens n+1 Replikate vor Commit kontaktier werden (HA nur bei n = 0)
Garantien C und D in verteilten Systemen benötigt ein Kommunikation bei Commit, zB. **2-Phasen-Commit Protokoll**, **Raft**, oder **Paxos**

- *Lost Update*: Konfliktlösung: Last-Write-Wins
- *Write Skew*: Lost Update generalisiert auf verschieden Objekte
- **2-Phasen-Commit Protokoll**

## CAP-Theorem
- **C**onsistency = Linearisierbarkeit
- **A**vailability = Verfügbarkeit
- **P**artition Tolerance: System funktioniert trotz Netzwerkpartitionierung
- Ein verteiltes System kann *maximal 2 der 3* Eigenschaften erfüllen

### Fälle
- CA: in nicht-verteilten Systemen: PostgreSQL, Redis
- CP: konsistent aber nicht verfügbar bei Netzwerkpartitionierung: MongoDB, HBase, Neo4j
- AP: Verfügbar aber nicht konsistent bei Netzwerkpartitionierung: CouchDB, Cassandra