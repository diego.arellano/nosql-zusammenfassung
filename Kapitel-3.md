# Kapitel 3: Graphdatenbanken
- Graph hat Knoten und Kanten
- Graph kann gerichtet oder ungerichtet sein
- Property Graph Modell:
	- gerichteter Multigraph
	- mit Beschriftung/Label
	- mit optionalen Attributen (properties) mittels Schlüssel-Wert-Paaren
	- Knoten und Kanten haben ID
	- schmafrei
- andere Modell: RDF
- Erweiterungen des Graph-Modells
	- zeitlich
	- räumlich
	- räumlich-zeitlich

## Graphrepräsentation
### Adjazenzmatrix
+ schneller Zugriff auf Kanten
- hoher Speicherbedarf |V|²
- ineffizientes Lesen aller Kanten eines Knoten

### Adjazenzliste
+ linearer Speicherplatz |V| + |E|
+ Zugriffszeit für alle Kanten eines Knoten abhängig von Anzahl lokal ausgehender Kanten, nicht von globalen Knotenanzahl
- aufwändigere Implemientierung als Adjazenzmatrix
- Prüfung  auf spezifische Kante aufwändiger

Neo4j implementiert eine **indexfreie Adjazenz**.

## Cypher
- CREATE: Erzeugen von Knoten, Kanten, Properties und Labels.
`CREATE (n1:Node {name:1}) -[:link {at: date("2023-05-04")}] -> (n2:Node {name:2})`
- MERGE: führt existierender und nicht existierender Teile eines Graphen zusammen.
`MERGE (n1:Node {name:1}) -[:link]-> (n3:Node {name:3})`
- SET: update von Properties und Labels
- DELETE: Entfernen von Knoten und Kanten
- REMOVE: Entfernen von Properties und Labels
- WITH: Verknüpfen mehrerer Anfragen
- CALL: Ausführen von Unterabfragen
- Anfragen
` MATCH (n:Node)->(m:Node)
WHERE n < m
RETURN m
ORDER BY m.attribute1
LIMIT 5
SKIP 1
`
	RETURN kann eine Tabelle oder ein Teilgraph sein

## Eigenschaften von Neo4j
- ACID Garantien
- Transaktion Replay nach Fehler
- In-memory Caching
- Lokalen Anfragen: Traversierung ausgehend von bestimmten Knoten

## Architektur von Neo4j
- Causal Cluster
- Replikation: Primary-Secondary
- Server kann gleichzeitig Primary für A und Secondary für B sein
- zahlreiche Read Replika für Skalierbarkeit von Leseanfragen
- kausale Konsistenz wird gewährleistet

### Causal Cluster
Primary Server:
- Annahme aller Schreibbefehle
- Replikation zwischen Core Servern über Raft Protokoll: Transaktion ist erfolgreich, falls sie von Mehrheit aller Core Server verarbeitet wurde
- Wahl eines Leaders: verantwortlich für alle Transaktionen
- verfügbar solange Mehrheit der Server funktioniert. Andernfalls bleibende Core Server Read-Only
Secondary Server:
- Read-Only Kopien der Core Server
- Asynchrone Synchronisation
- Kausale Konsistenz
	- RYW über Bookmarks
	- Nutzer erhält Bookmark beim Schreiben auf Core Server
	- die Server führt eine weitere Transaktion aus, nur wenn die Server die Transaktionen mit Bookmarks schon verarbeitet haben.

## Raft Protokoll
- **R**eplicated **a**nd **f**ault **t**olerant
- "Quorum-based consensus protocol"

### Leader Election
1. Ein Knoten kann in einem von 3 Zuständen sein:
- *Follower*
- *Candidate*
- *Leader*
2. alle Knoten beginnen im Follower Zustand.  
3. Falls Followers nicht von einem Leader hören, können sie Candidates werden.
4. Der Candidate fragt um Stimmen an.
5. Andere Knoten antworten.
6. Candidate wird Leader, falls er die Mehrheit der Stimmen kriegt.

#### Details
1. Es gibt zwei Timer, die eine Wahl bestimmen:
	- *Election Timeout*: Zeit, die ein Follower wartet, bevor er Candidat wird (zufällig zwischen 150 und 300 ms).
	- *hearbeat timeout*
2. Nach der Election Timeout wird ein Follower Candidate, wahlt sich selbst und fragt die anderen mit einem *Request Vote* an.
3. Falls der fragende Knoten noch nicht gewählt hat, dann wählt er den Candidate and resets sein Election Timeout.
4. Wenn ein Candidate die Mehrheit die Stimme bekommt, wird er Leader.
5. Der neuen Leader sendet *Append Entries* Nachrichten seiner Followers.
6. Diese Nachrichten sind in Intervale gesendet, die durch die *hearbeat timeout* spezifiert sind.
7. Followers antworten jede *Append Entries* Nachrichten
8. Dieser Zustand gilt, bis ein Follower keine *heartbeats* mehr bekommt und selbst Candidate wird.

#### Reelection
- Da ein Candidate die Mehrheit der Stimmen braucht, kann nur einen Leader gewählt werden.
- Split vote: zwei Candidate werden gleichtzeitig Leader werden. In diesem Fall wird die Leader Election wiederholt.
- Nur Knoten mit aktuellsten Log werden von anderen Knoten akzeptiert.

### Log Replication
1. Alle Änderungen zum System werden vom Leader bearbeitet
2. Jede Änderung ist als Entry in der Knotens Log addiert.
3. Vor Commit: Der Leader leitet die Änderung an die Followers weiter.
4. Leader wartet, bis eine Mehrheit von Knoten die Entry geschrieben haben.
5. Erst nun ist die Entry beim Leader committed.
6. Der Leader teilt der anderen Knoten mit, dass die Entry committed wurde.
7. Cluster is nun im Konsens (Followers kopieren Log vom Leader)

#### Details
1. Änderungen sind auch durch *Append Entries* Nachrichten weitergeleitet.
2. Erst sendet ein Client eine Änderung zu Leader.
3. Die Änderung wird in Leaders Log addiert.
4. Änderung wird bei nächsten *heartbeat* weitergeleitet.
5. Die Änderung wird erst committed, wenn eine Mehrheit der Followers zustimmen.
6. Leader sendet Nachricht zu Client

### Network Partition
1. Entsteht eine Partition, wird ein Leader in der neuen Partition gewahlt.
2. Jetzt gibt es zwei Leaders.
3. Jede Client ändert das System durch Nachrichten an die verschiedenen Leaders.
4. Der Leader, wer sich mit der Mehrheit der Knoten nicht kommunizieren kann, kann nie seine Änderung committen.
5. Daher wird eine Änderung stattfindet, wobei die Mehrheit der Knoten angefragt werden können.
6. Gäbe es nun keine Partition mehr, wird der Leader mit geringsten *election Term* zurücktreten.
7. Followers dieses alten Leader werden ihre Änderungen zurücksetzen (rollback) und die Log des neuen Leaders kopieren.
8. Log ist nun im Cluster konsistent.